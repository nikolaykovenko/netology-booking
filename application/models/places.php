<?php
/**
 * @package netology_booking
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 08.04.14
 */

/**
 * Модель места в зале
 */
class Places extends MY_Model {

 /**
  * Добавляет место в зале кинотеатра
  * @param int $row ряд
  * @param int $column место
  * @param bool $double_places возможность купить только два места рядом
  * @return FALSE|int ID места в случае успеха
  */
 public function add($row, $column, $double_places = FALSE)
 {
  if ($this->get_place($row, $column)) return FALSE;
  $this->db->insert($this->table_name(), array('row'=>$row, 'column'=>$column, 'double_places'=>$double_places));
  
  return $this->db->insert_id();
 }

 /**
  * Возвращает запись места из БД по номеру ряда и места 
  * @param int $row
  * @param int $column
  * @return null|stdClass
  */
 public function get($row, $column)
 {
  $result = $this->db->where(array('row'=>$row, 'column'=>$column))->get($this->table_name())->result();
  if (empty($result)) return NULL; else return $result;
 }

 /**
  * Возвращает список мест из БД по номеру ряда
  * @param int $row
  * @return null|array
  */
 public function get_row_places($row)
 {
  $result = $this->db->where(array('row'=>$row))->get($this->table_name())->result();
  if (empty($result)) return NULL; else return $result;
 }

 /**
  * Возвращает список всех мест
  * @return null|array
  */
 public function get_all_places()
 {
  $result = $this->db->order_by('row, column')->get($this->table_name())->result();
  if (empty($result)) return NULL; else return $result;
 }

 /**
  * Принимает массив id мест, выберает только те из них, которые можно бронировать только парами
  * @param array $places_ids массив id мест
  * @return null|array полную информацию из БД
  */
 public function select_double_places($places_ids = array())
 {
  $result = $this->db->order_by('row, column')->where(array('double_places'=>1))->where_in('id', $places_ids)->get($this->table_name())->result();
  if (empty($result)) return NULL; else return $result;
 }

 /**
  * Преобразует массив $places в двумерный массив для удобного вывода на плане зала
  * @param array $places массив мест
  * @return array
  */
 public function format_places_for_plan($places)
 {
  $result = array();
  foreach ($places as $line) $result[$line->row][$line->column] = $line;
  return $result;
 }

 /**
  * Устанавливает значение double_places для заданного ряда
  * @param int $row
  * @param bool $double_places
  * @return int количество обновленных мест
  */
 public function set_row_double($row, $double_places)
 {
  $this->db->update($this->table_name(), array('double_places'=>$double_places), array('row'=>$row));
  return $this->db->affected_rows();
 }
}