<?php
/**
 * @package netology_booking
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 08.04.14
 */

/**
 * Модель цен на сеансы 
 */
class Prices extends MY_Model {

 /**
  * Устанавливает цену на заданный сеанс для заданного места
  * @param int $event_id
  * @param int $place_id
  * @param float $price
  * @return int id записи
  */
 public function set_price($event_id, $place_id, $price)
 {
  $this->db->query("replace into `{$this->table_name()}` set `event`='".(int)$event_id."', `place`='".(int)$place_id."', `price`='".(float)$price."'");
  return $this->db->insert_id();
 }

 /**
  * Устанавливает цены на заданный сеанс для целого ряда мест
  * @param int $event_id
  * @param int $row
  * @param float $price
  * @return int количество обновленных цен
  */
 public function set_row_prices($event_id, $row, $price)
 {
  $this->load->model('places');
  $places = $this->places->get_row_places($row);
  $places_count = 0;
  
  foreach ($places as $place) if ($this->set_price($event_id, $place->id, $price)) $places_count++;
  
  return $places_count;
 }

 /**
  * Устанавливает цены для всех мест на сеанс
  * @param int $event_id
  * @param float $price
  * @return int количество обновленных цен
  */
 public function set_event_prices($event_id, $price)
 {
  $this->load->model('places');
  $places = $this->places->get_all_places();
  $places_count = 0;

  foreach ($places as $place) if ($this->set_price($event_id, $place->id, $price)) $places_count++;

  return $places_count;
 }

 /**
  * Возвращает цену на заданный сеанс для заданного места
  * 
  * Если не задана особая цена, возаращается стандартная
  * 
  * @param int $event_id
  * @param int $place_id
  * @return float
  */
 public function get_price($event_id, $place_id)
 {
  $price = $this->db->where(array('event'=>$event_id, 'place'=>$place_id))->get($this->table_name())->row();
  return $this->price($price->price);
 }

 /**
  * Возвращает двумерный массив с местами, добавляя у нему цены на билеты для текущего сеанса
  * @param int $event_id
  * @return array
  */
 public function get_event_prices($event_id)
 {
  $this->load->model('places');
  $places = $this->places->get_all_places();
  
  $prices = array();
  $prices_data = $this->db->where(array('event'=>$event_id))->get($this->table_name())->result();
  foreach ($prices_data as $price) $prices[$price->place] = $price->price;
  
  foreach ($places as $index => $place) $places[$index]->price = $this->price(array_key_exists($place->id, $prices) ? $prices[$place->id] : null);
  
  return $places;
 }


 /**
  * Получает преположительно запись цены в неизветсном формате, проверяет ее, если цена некорректна - возвращает стандартную цену на билет
  * @param stdClass|int|float|string|null $price
  * @return float
  */
 protected function price($price)
 {
  if (is_object($price) and property_exists($price, 'price')) return $price->price;
  elseif (($price = floatval($price)) > 0) return $price;
  else return $this->config->item('standard_ticket_price');
 }
}