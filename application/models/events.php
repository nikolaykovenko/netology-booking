<?php
/**
 * @package netology_booking
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 08.04.14
 */

/**
 * Модель сеанса
 */
class Events extends MY_Model {

 /**
  * Добавляет сеанс в расписание
  * @param int $film_id
  * @param timestamp|string $date_time дата и время сеанса
  * @return integer|FALSE id сеанса, или FALSE в случае, если новый сеанс пересекается с уже существуюшим
  */
 public function add($film_id, $date_time)
 {
  if (is_int($date_time)) $date_time = date("Y-m-d H:i", $date_time);
  
  if ($this->check_time_avail($date_time))
  {
   $this->db->insert($this->table_name(), array('film'=>$film_id, 'date'=>$date_time));
   return $this->db->insert_id();
  }
  
  return FALSE;
 }

 /**
  * Возвращает информацию о сеансе по ID
  * @param int $id
  * @param array $where дополнительные условия отбора
  * @return stdClass|null
  */
 public function get_event($id, $where = array())
 {
  $item = $this->db->where(array_merge(array('id'=>$id), $where))->get($this->table_name())->row();
  if (empty($item)) return NULL; else return $item;
 }

 /**
  * Возвращает список сеансов на фильм
  * @param int $film_id
  * @param NULL|array $where дополнительные условия отбора
  * @return array
  */
 public function get_film_events($film_id, $where = array())
 {
  return $this->db->where(array_merge(array('film'=>$film_id), $where))->order_by('date')->get($this->table_name())->result();
 }

 /**
  * Возвращает список актуальных сеансов на фильм
  * @param int $film_id
  * @return array
  */
 public function get_actual_film_events($film_id)
 {
  return $this->get_film_events($film_id, array('date >='=>date("Y-m-d H:i")));
 }

 /**
  * Проверяет, осталось ли до начала сеанса более 30 минут
  * @param int $event_id
  * @return bool
  */
 public function check_event_in_future($event_id)
 {
  $result = $this->db->query("select TIMESTAMPDIFF(minute, now(), date) as `m_to_start` from `{$this->table_name()}` where `id`='".(int)$event_id."'")->row();
  return is_object($result) and $result->m_to_start > 30;
 }

 
 
 /**
  * Проверка, не пересекается ли новый сеанс с уже существующим
  * 
  * В данном случае заглушка.
  * 
  * @param timestamp|string $date_time
  * @return bool
  */
 protected function check_time_avail($date_time)
 {
  return TRUE;
 }
}