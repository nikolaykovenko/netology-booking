<?php
/**
 * @package netology_booking
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 08.04.14
 */

/**
 * Обработчик заказов
 */
class Bookings extends MY_Model {

 /**
  * Получает массив мест, добавляет информацию об уже забронированных местах
  * @param int $event_id
  * @param array $places
  * @return array
  */
 public function add_booking_info($event_id, $places)
 {
  $booked_places = array();
  $data = $this->db->select('booking_places.*')->join($this->table_name(), $this->table_name().'.id = booking_places.booking')->where(array('event'=>$event_id))->get('booking_places')->result();
  foreach ($data as $line) $booked_places[] = $line->place;
  foreach ($places as $index => $place) $places[$index]->booked = (int)in_array($place->id, $booked_places);
  
  return $places;
 }

 /**
  * Бронирует переданный список мест
  * @param int $event_id сеанс
  * @param array $places массив id мест для бронирования
  * @param string $fio Ф.И.О. клиента
  * @return int ID бронироавния
  * @throws Exception в случае ошибки. Упрощенный вариант, в реальной жизни можно было бы использовать разные типы исключений, а так же более сложную и удобную систему оповещения об ошибках.
  * Решение с исключениями сознательно упрощено в виду специфики тестового задания.
  */
 public function book($event_id, $places, $fio)
 {
  $this->load->model('events');
  
  if (empty($fio) or empty($event_id) or empty($places)) throw new Exception('fill all the fields');
  if (!$this->check_double_places($places)) throw new Exception('Please, book double places in special rows.');
  if (!$this->events->check_event_in_future($event_id)) throw new Exception('Sorry, you can\'n book events less then for 30 minutes to start =(');
  
  foreach ($places as $place) if (!$this->check_avail_for_booking($event_id, $place)) throw new Exception('Place '.$place.' is booked!');
  
  $this->db->trans_start();
  $this->db->insert($this->table_name(), array('event'=>$event_id, 'fio'=>$fio));
  $booking_id = $this->db->insert_id();
  $insert = array();
  foreach ($places as $place) $insert[] = array('booking'=>$booking_id, 'place'=>$place);
  $this->db->insert_batch('booking_places', $insert);
  $this->db->trans_complete();
  if ($this->db->trans_status() === FALSE) throw new Exception('unknown error');
  
  return $booking_id;
 }

 /**
  * Подтверждение оплаты бронирования
  * @param int $booking_id
  * @return bool
  */
 public function submit($booking_id)
 {
  $info = $this->db->where(array('id'=>$booking_id))->get($this->table_name())->row();
  
  if (!empty($info))
  {
   if ($info->payed == 1) return TRUE;
   else
   {
    $this->db->where(array('id'=>$booking_id))->update($this->table_name(), array('payed'=>1));
    if ($this->db->affected_rows() == 1) return TRUE;
   }
  }
  
  return FALSE;
 }

 /**
  * Удаляет невыкупленные бронирования за 30 минут до начала сеанса
  * @return int количество удаленных бронирований
  */
 public function delete_bookings_before_30_minutes_to_begin()
 {
  $this->db->query("delete `{$this->table_name()}`.*
                    from `{$this->table_name()}` join `events` on `{$this->table_name()}`.`event`=`events`.`id`
                    where `{$this->table_name()}`.`payed`='0' and TIMESTAMPDIFF(minute, now(), `events`.`date`) <= 30");
  return $this->db->affected_rows();
 }

 /**
  * Удаляет невыкупленные за 1 час бронирования
  * @return int количество удаленных бронирований
  */
 public function delete_bookings_after_1h_after_booking()
 {
  $this->db->query("delete from `{$this->table_name()}` where `payed`=0 and TIMESTAMPDIFF(minute, `add_time`, now()) >= 60");
  return $this->db->affected_rows();
 }

 /**
  * Проверяет выполнение условия, при котором, места из определенных рядов можно бронировать только парами
  * @param array $places
  * @return bool
  */
 protected function check_double_places($places)
 {
  $this->load->model('places');
  $double_places = $this->places->select_double_places($places);
  
//  Если не было выбрано мест в специальных рядах - все ок
  if (is_null($double_places)) return TRUE;
  
  $double_places = $this->places->format_places_for_plan($double_places);
  foreach ($double_places as $row => $row_places)
  {
//   Если в ряде выбрано непарное количество парных мест - сразу фейл.
   if (count($row_places)%2) return FALSE;
  
   
//   Далее проверяем номера мест в ряде, итерация через одну. Если находим несоотвествие - фейл.
   $row_places = array_keys($row_places);
   $count = count($row_places);
   
   for ($i=0; $i<$count; $i+=2) if ($row_places[$i+1] - $row_places[$i] != 1) return FALSE;
  }
  
  return TRUE;
 }

 /**
  * Проверяет возможность забронировать место на определенный сеанс
  * @param int $event_id
  * @param int $place_id
  * @return bool
  * @throws Exception
  */
 protected function check_avail_for_booking($event_id, $place_id)
 {
  $count = $this->db->join('booking_places', $this->table_name().'.id = booking_places.booking')->where(array('event'=>$event_id, 'place'=>$place_id))->
                      from($this->table_name())->count_all_results();
    
  return $count == 0;
 }
}