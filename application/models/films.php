<?php
/**
 * @package netology_booking
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 08.04.14
 */

/**
 * Модель фильма
 */
class Films extends MY_Model {

 /**
  * Добавляет новый фильм в БД
  * @param string $caption
  * @param string $describe
  * @return int id записи
  */
 public function add($caption, $describe)
 {
  $this->db->insert($this->table_name(), array('caption'=>$caption, 'describe'=>$describe));
  return $this->db->insert_id();
 }

 /**
  * Возвращает список всех фильмов
  * @return array
  */
 public function get_all()
 {
  return $this->db->order_by('id', 'desc')->get($this->table_name())->result();
 }

 /**
  * Возвращает список запланированных к показу фильмов
  * @return array
  */
 public function get_actual()
 {
  return $this->db->select($this->table_name().'.*')->join('events', $this->table_name().'.id = events.film')->where("date >= now()")->group_by($this->table_name().'.id')->order_by('date')->get($this->table_name())->result();
 }

 /**
  * Возвращает подробную информацию о фильме
  * 
  * В том числе, список актуальных сеансов на фильм
  * 
  * @param int $film_id
  * @return stdClass|null
  */
 public function film_info($film_id)
 {
  $film = $this->db->where(array('id'=>$film_id))->get($this->table_name())->row();
  if (!empty($film))
  {
   $this->load->model('events');
   $film->events = $this->events->get_actual_film_events($film->id);
   return $film;
  }
  
  return NULL;
 }
}