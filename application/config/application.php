<?php
/**
 * @package NCMS
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 04.12.13
 */

/**
 * Настройки приложения
 */

/**
 * Шаблон, который нужно отображать саомостоятельно, без обертки контейнера
 */
$config['independent_templates'] = array('container');

/**
 * Шаблон контеннера
 */
$config['container_template'] = 'container.tpl';

/**
 * Список переменных, которые нужно автоматически передавать в шаблоны
 */
$config['autoload_template_variables'] = array('design_dir', 'app_caption');

/**
 * Директория, в которой размещены файлы представления 
 */
$config['design_dir'] = '/design/';

/**
 * Название проекта
 */
$config['app_name'] = 'netology_booking';

/**
 * Короткое описание проекта
 */
$config['app_caption'] = 'Тестовое задание №5';

/**
 * Стандартная цена билета, если не задана специальная цена для нужного места и сеанса
 */
$config['standard_ticket_price'] = 45;
?>