<?php
/**
 * @package netology_booking
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 08.04.14
 */

/**
 * Расширенная модель
 */
class MY_Model extends CI_Model {

 /**
  * Возвращает имя таблицы модели в БД
  * @return string
  */
 public function table_name()
 {
  return strtolower(get_class($this));
 }

 /**
  * Стандартный метод удаления записи в БД
  * 
  * Для более сложных моделей должна быть переопределена дополнена кодом для удаления изображений и других связанных объектов 
  * 
  * @param int $id
  * @return int количество удаленных записей
  */
 public function delete($id)
 {
  $this->db->where('id', $id)->delete($this->table_name());
  return $this->db->affected_rows();
 }

} 