<?php
/**
 * @package netology_booking
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 28.11.13
 */

/**
 * Расширенный контроллер
 */
abstract class MY_Controller extends CI_Controller {
 /**
  * @var array массив переменных, которые нужно передать в шаблон
  */
 protected $variables = array();

 public function __construct()
 {
  parent::__construct();
  
  $variables = $this->config->item('autoload_template_variables');
  foreach ($variables as $variable) $this->add_template_variable($variable, $this->config->item($variable));
  
  $this->add_template_variable('actual_films', $this->films->get_actual());

  $CI = &get_instance();
  $this->add_template_variable('CI', $CI);
 }

 /**
  * Добавляет переменную в шаблон
  * @param mixed $key
  * @param mixed $value
  */
 public function add_template_variable($key, $value)
 {
  $this->variables[$key] = $value;
 }

 /**
  * Возвращает массив переменных, которые нужно передать в шаблон
  * @return array
  */
 public function get_variables()
 {
  return $this->variables;
 }

 /**
  * Возвращает переменную key
  * @param $key
  * @return mixed
  */
 public function get_variable($key)
 {
  if (array_key_exists($key, $this->variables)) return $this->variables[$key]; else return NULL;
 }

 /**
  * Отображает шаблон
  * @param string $template имя шаблона
  * @param array $variables дополнительные переменные, которые нужно передать в шаблон
  * @param bool $simple_template флаг отображения шаблона без обертки контенером
  */
 public function display($template, $variables = array(), $simple_template = FALSE)
 {
  foreach ($variables as $key => $value) $this->add_template_variable($key, $value);
  $path = pathinfo($template);

  if ($simple_template == TRUE or !in_array($path['filename'], $this->config->item('independent_templates'))) {
   $this->add_template_variable('template', $template);
   $template = $this->config->item('container_template');
  }

  $this->parser->parse($template, $this->get_variables());
 }
}  