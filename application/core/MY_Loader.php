<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 28.11.13
 * Time: 9:36
 */

class MY_Loader extends CI_Loader {
 public function __construct() {
  parent::__construct();
  $this->_ci_view_paths = array(FCPATH.'design/templates/' => TRUE,  APPPATH.'views/'	=> TRUE);
 }
} 