<?php
/**
 * @package netology_booking
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 08.04.14
 */

class Start extends MY_Controller {

 public function index()
 {
  $this->display('message.tpl', array('caption'=>'Бронирование мест', 'message'=>'Пожалуйста, выберите интересующий Вас фильм'));
 }
}