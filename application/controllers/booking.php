<?php
/**
 * @package netology_booking
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 08.04.14
 */


/**
 * Контроллер, отвечаюищий за бронирование мест
 */
class Booking extends MY_Controller {

 /**
  * Вывод доступных сеансов для фильма
  * @param int $film_id
  */
 public function index($film_id)
 {
  $film = $this->films->film_info($film_id);
  if (empty($film)) show_404();
  
  $this->add_template_variable('caption', 'Бронирование мест на фильм "'.$film->caption.'"');
  $this->add_template_variable('film', $film);
  
  $this->display('events-list.tpl');
 }

 /**
  * Выводит страницу выбора мест и заполнения анкеты
  * @param int $event_id
  */
 public function select_place($event_id)
 {
  $this->load->model('events');
  $event = $this->events->get_event($event_id);
  if (empty($event)) show_404();
  $film = $this->films->film_info($event->film);
  
  $this->load->model('prices');
  $this->load->model('places');
  $this->load->model('bookings');
  
  $places = $this->places->format_places_for_plan($this->bookings->add_booking_info($event_id, $this->prices->get_event_prices($event_id)));
  
  $this->add_template_variable('caption', 'Бронирование мест на фильм "'.$film->caption.'", сеанс '.date("m.d H:i", strtotime($event->date)));
  $this->add_template_variable('event', $event_id);
  $this->add_template_variable('places', $places);

  $this->display('places-list.tpl');
 }

 /**
  * Получает данные и вызывает методы бронирования
  */
 public function book()
 {
  $this->load->model('bookings');
  
  try {
   $booking_id = $this->bookings->book($this->input->post('event'), $this->input->post('places'), $this->input->post('fio'));
   $result = array('status'=>'success', 'message'=>'Места успешно забронированы, ID заказа - '.$booking_id);
  }
  catch (Exception $e)
  {
   $result = array('status'=>'error', 'message'=>$e->getMessage());
  }
  
  echo json_encode($result);
 }

 /**
  * Подтверждает оплату бронирования
  * 
  * Упрощенный вариант. Теоретически, это страница, на которую платежная система отправляет результат оплаты. 
  * 
  * @param int $booking_id
  */
 public function submit($booking_id)
 {
  $this->load->model('bookings');
  $caption = 'Подтверждение оплаты бронирования';
  
  if ($this->bookings->submit($booking_id)) $text = 'Процес оплаты бронирования №'.$booking_id.' успешно завершен'; else $text = 'Неизвестная ошибка =(';
  
  $this->display('message.tpl', array('caption'=>$caption, 'message'=>$text));
 }
}