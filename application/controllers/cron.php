<?php
/**
 * @package netology_booking
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 09.04.14
 */

/**
 * Контроллер, отвечающий за крон
 */
class Cron extends MY_Controller {

 public function index()
 {
  $this->load->model('bookings');
  $count = $this->bookings->delete_bookings_before_30_minutes_to_begin() + $this->bookings->delete_bookings_after_1h_after_booking();
  
  echo 'Bookings deleted: '.$count.'.';
 }
 
}