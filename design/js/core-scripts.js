function translit(word)
{
 var from = Array('А','а','Б','б','В','в','Г','г','Ґ','ґ','Д','д','Е','е','Є','є','Ж','ж','З','з','И','и','І','і','Ї','ї','Й','й','К','к','Л','л','М','м','Н','н','О','о','П','п','Р','р','С','с','Т','т','У','у','Ф','ф','Х','х','Ц','ц','Ч','ч','Ш','ш','Щ','щ','Ю','ю','Я','я','Ь','ь','Ы','ы',' ','Ъъ','-','Э','э'),
     translate = Array('A','a','B','b','V','v','Gh','gh','G','g','D','d','E','e','Je','je','Zh','zh','Z','z','I','i','I','i','Ji','ji','J','j','K','k','L','l','M','m','N','n','O','o','P','p','R','r','S','s','T','t','U','u','F','f','Kh','kh','C','c','Ch','ch','Sh','sh','Shh','shh','Ju','ju','Ja','ja','J','j','Y','y',' ','','-','E','e'),
     result='', word_index = 0, code = 0;
 
 for(var i=0;i<word.length;i++)
 {
  if ((word_index = (jQuery.inArray(word[i], from))) != -1) result += translate[word_index];
  else
  if ((code = word.charCodeAt(i)) && ((code >= 48 && code <= 57) || (code >= 65 && code <= 90) || (code >= 97 && code <= 122))) result += word.charAt(i);
 }

 return result;
}


$(function() {
 if (!Modernizr.inputtypes.date)
 {
  $("input[type='date']").datepicker({
   dateFormat: 'yy-mm-dd',
   showButtonPanel: true,
   changeMonth: true,
   changeYear: true
  });
 }
});