/**
 * Created by user on 09.04.14.
 */

function get_active_places()
{
 var result = [];
 $('#cinema-plan .active[data-place]').each(function() { result.push($(this).attr('data-place'));  });
 return result;
}

function book()
{
 
}


$(function() {
 $('#cinema-plan [data-place]').click(function() {
  $(this).toggleClass('active');
  if (get_active_places().length > 0) $('#booking-form').slideDown(100); else $('#booking-form').slideUp(100);
 });
 
 $('#booking-form').submit(function(e) {
  e.preventDefault();
  
  $.post('/booking/book', {'event': $(this).attr('data-event'), 'fio': $(this).find('[name="fio"]').val(), 'places': get_active_places() },
   function(data) {
    var result = $.parseJSON(data);
    
    switch (result.status)
    {
     case 'success': $('#booking-form').html(result.message); break;
     default: alert(result.message);
    }
   });
 });
})