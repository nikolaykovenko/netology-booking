{* Smarty *}
<!DOCTYPE html>
<html>
<head>
 <meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
 <title>{if $caption}{$caption} - {/if}{$app_caption}</title>
 <link href="{$design_dir}bootstrap/css/bootstrap.css" rel="stylesheet">
 <link href="{$design_dir}css/style.css" rel="stylesheet">
 <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
 <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
 <!--[if lt IE 9]>
 <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
 <![endif]-->
 <script src="{$design_dir}js/jquery-1.10.2.min.js"></script>
 <script src="{$design_dir}bootstrap/js/bootstrap.min.js"></script>
 <script src="{$design_dir}js/modernizr.custom.09116.js"></script>
 <script src="{$design_dir}js/booking.js"></script>
</head>
<body data-admin="true">