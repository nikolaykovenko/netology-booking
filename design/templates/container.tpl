{* Smarty *}
{include file="header.tpl"}
<div class="container">
 <div class="row">
  <div class="col-sm-3 col-lg-2">{include file="main-nav.tpl"}</div>
  <div class="col-sm-9 col-lg-10">
   {if $caption}<h1 class="caption">{$caption}</h1>{/if}
   {if $template}{include file=$template}{/if}
  </div>
 </div>
</div>
{include file="footer.tpl"}
