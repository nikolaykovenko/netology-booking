{* Smarty *}
{if $film->events}
 <h3>Пожалуйста, выберите интересующий Вас сеанс на фильм:</h3>
 <ul class="events-list list-inline">
  {foreach from=$film->events item="event"}
   <li><a href="/booking/select_place/{$event->id}">{$event->date|date_format:"%d.%m %H:%M"}</a></li>
  {/foreach}
 </ul>
{else}
 К сожалению, на данный фильм нет доступных сеансов
{/if}
