{* Smarty *}
<section class="places-list" id="cinema-plan">
 {foreach from=$places key="row_num" item="row"}
  <div class="btn-group btn-group-xs">
   <span class="btn btn-default disabled row-label"> Ряд {$row_num}</span>
   {foreach name="rows" from=$row item="place"}
    <button data-place="{$place->id}" type="button" class="btn btn-default{if $place->booked} disabled{/if}{if $place->double_places} double-place" title="Только для заказа парами{/if}">
     {$place->price}
    </button>
   {/foreach}
  </div>
 {/foreach}
</section>
<div class="row">
 <div class="col-md-8 col-lg-6">
  <form id="booking-form" role="form" data-event="{$event}">
   <h3 class="caption">Быстрое бронирование:</h3>
   <div class="form-group">
    <label for="fio">Ф.И.О.</label>
    <input type="text" class="form-control" id="fio" name="fio" value="Иванов Иван" required>
   </div>
   <button type="submit" class="btn btn-default">Забронировать</button>
  </form>
 </div>
</div>
